package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// ControllerInterface 定义控制器接口
type ControllerInterface interface {
	Response(c *gin.Context, data interface{})
}

// BaseController 实现了 ControllerInterface 接口的基础控制器
type BaseController struct{}

// Response 成功响应
func (bc *BaseController) Response(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{"data": data})
}

// UserController 用户控制器，实现了 ControllerInterface 接口
type UserController struct {
	*BaseController // 匿名字段
}

// List 显示所有用户
func (uc *UserController) List(c *gin.Context) {
	uc.Response(c, "List of users--taoyuan")
}

// main函数是程序的入口点。
func main() {
	// 初始化gin路由器，使用默认的中间件。
	router := gin.Default()

	// 初始化UserController，它继承自BaseController。
	userCtrl := &UserController{&BaseController{}}

	// 创建用户相关的路由组，以"/users"开头。
	userRoutes := router.Group("/users")
	{
		// 定义GET请求的路由处理函数，处理用户列表的请求。
		userRoutes.GET("", userCtrl.List)
	}

	// 启动HTTP服务器，监听8080端口。
	router.Run(":8080")
}
