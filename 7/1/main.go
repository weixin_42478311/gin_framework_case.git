package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// UserController 定义用户控制器结构体
type UserController struct{}

// NewUserController 创建新的用户控制器实例
func NewUserController() *UserController {
	return &UserController{}
}

// Index 用户列表页面
func (uc *UserController) Index(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "User List"})
}

// Show 显示单个用户信息
func (uc *UserController) Show(c *gin.Context) {
	id := c.Param("id")
	c.JSON(http.StatusOK, gin.H{"user_id": id})
}

func main() {
	router := gin.Default()

	// 创建用户控制器实例
	userController := NewUserController()

	// 定义路由组
	userRoutes := router.Group("/users")
	{
		userRoutes.GET("", userController.Index)
		userRoutes.GET("/:id", userController.Show)
	}

	// 启动HTTP服务，默认在0.0.0.0:8080启动服务
	router.Run()
}
