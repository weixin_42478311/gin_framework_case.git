package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// AuthController 身份验证控制器
type AuthController struct{}

// AuthResponse 返回认证信息
func (ac *AuthController) AuthResponse(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"auth": "authenticated"})
}

// BaseController 基础控制器
type BaseController struct {
	*AuthController // 包含身份验证控制器
}

// Response 成功响应
func (bc *BaseController) Response(c *gin.Context, data interface{}) {
	bc.AuthResponse(c)
	c.JSON(http.StatusOK, gin.H{"data": data})
}

// UserController 用户控制器，包含基础控制器
type UserController struct {
	*BaseController // 包含基础控制器
}

// List 显示所有用户
func (uc *UserController) List(c *gin.Context) {
	uc.Response(c, "List of users")
}

// main函数是程序的入口点。
func main() {
	// 初始化gin路由器。
	router := gin.Default()

	// 初始化认证控制器。
	authCtrl := &AuthController{}

	// 初始化基础控制器，并传入认证控制器。
	baseCtrl := &BaseController{authCtrl}

	// 初始化用户控制器，并传入基础控制器。
	userCtrl := &UserController{baseCtrl}

	// 创建用户路由组。
	userRoutes := router.Group("/users")
	{
		// 定义用户列表路由。
		userRoutes.GET("", userCtrl.List)
	}

	// 启动服务器，监听8080端口。
	router.Run(":8080")
}
