package main

import (
	"gin_7_3/controllers"

	"github.com/gin-gonic/gin"
)

// main函数是程序的入口点。
func main() {
	// 初始化gin默认路由器。
	router := gin.Default()

	// 创建一个新的用户控制器实例。
	userCtrl := controllers.NewUserController()

	// 创建用户路由组。
	userRoutes := router.Group("/users")

	// 定义用户相关的路由和处理函数。
	{
		// 注册处理用户列表请求的路由。
		userRoutes.GET("", userCtrl.List)
		// 注册处理用户详情请求的路由。
		userRoutes.GET("/:id", userCtrl.Detail)
	}

	// 启动HTTP服务器，监听端口8080。
	router.Run(":8080")
}
