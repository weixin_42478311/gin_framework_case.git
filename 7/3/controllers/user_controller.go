// controllers/user_controller.go
package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// UserController 用户控制器
type UserController struct{}

// NewUserController 创建新的用户控制器实例
func NewUserController() *UserController {
	return &UserController{}
}

// List 显示所有用户
func (uc *UserController) List(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "List of users"})
}

// Detail 显示单个用户详情
func (uc *UserController) Detail(c *gin.Context) {
	id := c.Param("id")
	c.JSON(http.StatusOK, gin.H{"user_id": id})
}
