package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// BaseController 基础控制器
type BaseController struct{}

// Response 成功响应
func (bc *BaseController) Response(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{"data": data})
}

// UserController 用户控制器，继承自 BaseController
type UserController struct {
	BaseController // 匿名字段
}

// List 显示所有用户
func (uc *UserController) List(c *gin.Context) {
	uc.Response(c, "List of users")
}

// main函数是程序的入口点。
func main() {
	// 初始化gin默认路由器。
	router := gin.Default()

	// 实例化用户控制器。
	userCtrl := &UserController{}

	// 创建用户路由组，基路径为"/users"。
	userRoutes := router.Group("/users")
	{
		// 定义GET请求的路由规则，处理用户列表请求。
		userRoutes.GET("", userCtrl.List)
	}

	// 启动服务器，监听端口为8080。
	router.Run(":8080")
}
