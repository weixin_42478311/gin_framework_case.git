package main

import (
	"net/http" // 导入 net/http 包用于 HTTP 状态码

	"github.com/gin-gonic/gin" // 导入 Gin Web 框架
)

// UserController 用户控制器结构体，用于处理与用户相关的 HTTP 请求。
type UserController struct{}

// NewUserController 创建并返回一个新的用户控制器实例。
func NewUserController() *UserController {
	return &UserController{}
}

// List 处理获取所有用户的 HTTP GET 请求。
// 该方法接收一个 gin.Context 参数，用于处理请求和响应。
func (uc *UserController) List(c *gin.Context) {
	// 返回 JSON 格式的成功响应，包含消息 "List of users"
	c.JSON(http.StatusOK, gin.H{"message": "List of users"})
}

// Detail 处理获取单个用户详情的 HTTP GET 请求。
// 该方法接收一个 gin.Context 参数，用于处理请求和响应。
func (uc *UserController) Detail(c *gin.Context) {
	// 从 URL 参数中提取用户 ID
	id := c.Param("id")
	// 返回 JSON 格式的成功响应，包含用户 ID
	c.JSON(http.StatusOK, gin.H{"user_id": id})
}

func main() {
	// 创建默认的 Gin 路由引擎实例
	router := gin.Default()

	// 创建用户控制器实例
	userCtrl := NewUserController()

	// 定义路由组 "/users"，所有与用户相关的路由都将在这个路径下
	userRoutes := router.Group("/users")
	{
		// 将 HTTP GET 请求 "/" 映射到用户控制器的 List 方法，用于列出所有用户
		userRoutes.GET("", userCtrl.List)
		// 将 HTTP GET 请求 "/:id" 映射到用户控制器的 Detail 方法，用于显示特定用户的详情
		userRoutes.GET("/:id", userCtrl.Detail)
	}

	// 启动 HTTP 服务器，在端口 8080 上监听并服务请求
	router.Run(":8080")
}
