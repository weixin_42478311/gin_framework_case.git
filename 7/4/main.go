package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// AuthMiddleware 简单的身份验证中间件
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 设置或修改请求头
		c.Request.Header.Set("Authorization", "Bearer your-token-here")
		// 获取请求头中的Authorization字段
		token := c.GetHeader("Authorization")
		fmt.Println("Authorization:", token)
		if token == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			c.Abort() // 中断后续处理
			return
		}
		// 这里可以添加更复杂的验证逻辑，比如解码 JWT 令牌等
		c.Next() // 继续执行下一个中间件或处理器
	}
}

// UserController 用户控制器结构体，用于处理与用户相关的 HTTP 请求。
type UserController struct{}

// NewUserController 创建并返回一个新的用户控制器实例。
func NewUserController() *UserController {
	return &UserController{}
}

// List 处理获取所有用户的 HTTP GET 请求。
func (uc *UserController) List(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "List of users"})
}

// Detail 处理获取单个用户详情的 HTTP GET 请求。
func (uc *UserController) Detail(c *gin.Context) {

	id := c.Param("id")
	c.JSON(http.StatusOK, gin.H{"user_id": id})
}

func main() {
	router := gin.Default()

	// 创建用户控制器实例
	userCtrl := NewUserController()

	// 定义不带中间件的公共路由
	publicRoutes := router.Group("/public")
	{
		publicRoutes.GET("/ping", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "pong"})
		})
	}

	// 定义带中间件的受保护路由
	protected := router.Group("/users", AuthMiddleware())
	{
		protected.GET("", userCtrl.List)
		protected.GET("/:id", userCtrl.Detail)
	}

	// 启动 HTTP 服务器，在端口 8080 上监听并服务请求
	router.Run(":8080")
}
