package main

import (
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
)

// requestCount 记录HTTP请求的次数
var (
	requestCount int
	mu           sync.Mutex
)

// RequestCounter 是一个中间件函数，用于在每次HTTP请求时增加请求计数
// 该函数首先锁定互斥锁以确保并发安全，然后增加请求计数，并将计数存储在gin.Context中
// RequestCounter 返回一个中间件函数，用于统计HTTP请求的数量。
// 该中间件使用了互斥锁确保并发安全，特别是在处理高并发请求时。
// 它在每个请求的上下文中设置了当前的请求计数，供后续处理使用。
func RequestCounter() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 上锁以确保并发安全
		mu.Lock()
		// 增加请求计数
		requestCount++
		// 在上下文中设置当前的请求计数
		c.Set("requestCount", requestCount)
		// 解锁以释放资源
		mu.Unlock()
		// 调用链中的下一个处理函数
		c.Next()
	}
}

func main() {
	r := gin.Default()

	// 使用RequestCounter中间件来跟踪和增加请求计数
	r.Use(RequestCounter())

	// 处理/count路由的GET请求，返回当前请求计数
	r.GET("/count", func(c *gin.Context) {
		if count, exists := c.Get("requestCount"); exists {
			c.JSON(http.StatusOK, gin.H{"count": count})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Request count not found"})
		}
	})

	// 启动HTTP服务器，监听8080端口
	r.Run(":8080")
}
