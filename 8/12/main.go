package main

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

// QueryParamProcessor 是一个中间件函数，用于处理查询参数。
// 它从请求中获取 'search' 查询参数，
// 对其进行一些处理（例如去除首尾空白字符），
// 并将处理后的结果存储在上下文中，键为 'processedQuery'。
// 这样后续的处理器可以访问到处理后的查询参数。
func QueryParamProcessor() gin.HandlerFunc {
	return func(c *gin.Context) {
		query := c.Query("search")
		processedQuery := strings.TrimSpace(query) // 假设这里有一些复杂的处理逻辑
		fmt.Println("Processed query:", processedQuery)
		c.Set("processedQuery", processedQuery)
		c.Next()
	}
}

func main() {
	r := gin.Default()

	// 使用全局中间件 QueryParamProcessor。
	// 该中间件会预处理所有传入请求的查询参数，
	// 使任何后续路由处理器都可以访问处理后的查询参数。
	r.Use(QueryParamProcessor())

	// 定义 '/search' 的 GET 路由。
	// 该路由检查上下文中是否存在 'processedQuery'。
	// 如果存在，则返回包含处理后查询参数的搜索结果；
	// 否则，返回一条消息表示未提供查询参数。
	r.GET("/search", func(c *gin.Context) {
		if query, exists := c.Get("processedQuery"); exists {
			c.JSON(http.StatusOK, gin.H{"search_results": "Results for: " + query.(string)})
		} else {
			c.JSON(http.StatusOK, gin.H{"search_results": "No query provided"})
		}
	})

	// 启动 Gin 服务器，并监听 8080 端口。
	r.Run(":8080")
}
