package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// PerformanceMonitor 是一个中间件函数，用于监控请求的性能。
// 它会记录每个请求的处理时间。
func PerformanceMonitor() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()

		// 继续处理请求链中的其他处理器和中间件
		c.Next()

		// 请求处理后的操作
		end := time.Now()
		latency := end.Sub(start)

		log.Printf("Request completed in %v\n", latency)
	}
}

func main() {
	r := gin.Default()

	// 添加全局性能监控中间件
	r.Use(PerformanceMonitor())

	// 定义一些路由
	r.GET("/slow", func(c *gin.Context) {
		time.Sleep(5 * time.Second) // 模拟耗时操作
		c.JSON(http.StatusOK, gin.H{
			"message": "This is a slow endpoint",
		})
	})

	r.Run(":8080")
}
