package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// Logger 返回一个日志中间件，用于记录HTTP请求的详细信息。
// 该中间件主要记录了请求的路径、查询字符串、响应状态码、客户端IP和请求处理时间。
// 它通过gin.HandlerFunc类型返回一个闭包函数，以便在Gin路由中使用。
func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 记录请求开始时间
		start := time.Now()
		// 获取请求路径
		path := c.Request.URL.Path
		// 获取查询字符串
		raw := c.Request.URL.RawQuery

		// 处理请求前
		c.Next() // 处理请求

		// 处理请求后
		// 记录请求结束时间
		end := time.Now()
		// 计算请求处理耗时
		latency := end.Sub(start)
		// 获取HTTP响应状态码
		statusCode := c.Writer.Status()
		// 获取客户端IP
		clientIP := c.ClientIP()

		// 打印日志信息
		log.Printf("| %3d | %13v | %15s | %40s |",
			statusCode, latency, clientIP, path+"?"+raw)
	}
}

func main() {
	// 初始化Gin默认引擎
	r := gin.Default()

	// 使用Logger中间件
	r.Use(Logger())

	// 定义一个模拟耗时操作的测试路由
	r.GET("/test", func(c *gin.Context) {
		// 模拟耗时操作
		time.Sleep(5 * time.Second)
		// 返回JSON响应
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	// 启动HTTP服务器，监听端口8080
	r.Run(":8080")
}
