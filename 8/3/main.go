package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// CustomRecovery 自定义恢复中间件，用于捕获并处理panic
// 这个中间件的目的是当发生panic时，能够优雅地恢复并返回500错误响应
func CustomRecovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// 如果有 panic 发生，设置状态码为 500 并返回错误信息
				c.Error(err.(error)).SetMeta("recovered")
				c.AbortWithStatusJSON(http.StatusInternalServerError,
					gin.H{"error": "Internal Server Error"})
			}
		}()
		c.Next()
	}
}

func main() {
	r := gin.Default()

	// 使用自定义的恢复中间件
	r.Use(CustomRecovery())

	// 设置一个路由，访问该路由会触发panic
	r.GET("/panic", func(c *gin.Context) {
		panic("an error occurred!")
		// fmt.Print("This line will not be executed.")
	})
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{"message": "Hello, World!"})
	})

	// 启动服务器，监听8080端口
	r.Run(":8080")
}
