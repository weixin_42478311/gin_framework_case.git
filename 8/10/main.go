package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// AuthMiddleware 是一个中间件，用于验证请求的授权信息。
// 它通过检查请求头中的 Authorization 字段来确定请求是否经过授权。
// 如果验证通过，它会设置用户信息并允许请求继续进行；
// 如果验证失败，它会返回 401 状态码和错误信息。
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("Authorization")
		if token == "my-secret-token" {
			c.Set("user", "authenticated-user") // 设置用户信息
			c.Next()
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			c.Abort()
		}
	}
}

func main() {
	r := gin.Default()

	// 创建一个受保护的路由组，所有该组内的请求都需要经过授权验证。
	protected := r.Group("/protected", AuthMiddleware())
	{
		// 处理 /protected/profile 路径的 GET 请求。
		// 如果请求已授权，返回欢迎信息；否则，返回错误信息。
		protected.GET("/profile", func(c *gin.Context) {
			if user, exists := c.Get("user"); exists {
				c.JSON(http.StatusOK, gin.H{"message": "Welcome " + user.(string)})
			} else {
				c.JSON(http.StatusInternalServerError, gin.H{"error": "User info not found"})
			}
		})
	}

	// 启动 HTTP 服务器，监听 8080 端口。
	r.Run(":8080")
}
