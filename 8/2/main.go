package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// AuthRequired 是一个中间件函数，用于验证请求是否具有有效的授权令牌。
// 它通过检查HTTP请求的头部中的 "Authorization" 字段是否匹配预设的令牌来实现。
// 如果令牌无效，它会中止请求并返回 "Unauthorized" 错误。
// 如果令牌有效，它会允许请求继续进行到下一个处理函数。
func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Request.Header.Set("Authorization", "my-secret-token")
		token := c.GetHeader("Authorization")
		fmt.Println("Token:", token)
		if token != "my-secret-token" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			c.Abort()
			return
		}
		c.Next()
	}
}

func main() {
	r := gin.Default()

	// 应用认证中间件到所有 /admin 路由
	// 这里通过将 AuthRequired 作为参数传递给 r.Group 来实现，
	// 确保所有以 "/admin" 开头的路由都受到保护，防止未经授权的访问。
	admin := r.Group("/admin", AuthRequired())
	{
		// 定义 /admin/dashboard 路由的处理函数。
		// 该函数仅在通过 AuthRequired 中间件的验证后执行，
		// 确保只有拥有有效令牌的请求才能访问dashboard。
		admin.GET("/dashboard", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "Welcome to the dashboard"})
		})
	}

	// 启动HTTP服务器，监听端口8080。
	r.Run(":8080")
}
