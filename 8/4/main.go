package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// GlobalLogger 返回一个gin.HandlerFunc类型的中间件，用于全局日志记录
// 它记录了请求的来源IP、路径、查询参数以及请求处理时间
func GlobalLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 记录请求开始时间
		start := time.Now()
		// 获取请求路径
		path := c.Request.URL.Path
		// 获取查询参数
		raw := c.Request.URL.RawQuery
		// 获取客户端IP
		clientIP := c.ClientIP()

		// 日志记录：开始处理请求
		log.Printf("Start handling request from %s: %s?%s", clientIP, path, raw)

		// 继续处理请求链中的其他处理器和中间件
		c.Next()

		// 记录请求结束时间
		end := time.Now()
		// 计算请求处理耗时
		latency := end.Sub(start)
		// 获取HTTP响应状态码
		statusCode := c.Writer.Status()

		// 日志记录：请求处理结果，包括状态码、耗时、客户端IP和请求路径
		log.Printf("| %3d | %13v | %15s | %40s |\n",
			statusCode, latency, clientIP, path+"?"+raw)
	}
}

func main() {
	// 创建默认的gin路由器
	r := gin.Default()

	// 添加全局中间件：使用GlobalLogger记录所有请求的信息
	r.Use(GlobalLogger())

	// 定义路由：GET请求到/ping路径
	r.GET("/ping", func(c *gin.Context) {
		// 模拟耗时操作：休眠2秒
		time.Sleep(2 * time.Second)
		// 响应JSON数据：{"message": "pong"}
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	// 启动服务器，监听8080端口
	r.Run(":8080")
}
