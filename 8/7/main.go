package main

import (
	"github.com/gin-gonic/gin"
)

// VersionMiddleware 返回一个中间件处理器，用于在上下文中设置API版本信息。
// 参数 version 指定要设置的API版本。
// 该中间件使得后续的处理器可以访问通过c.Get("api-version")获取版本信息。
func VersionMiddleware(version string) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("api-version", version)
		c.Next()
	}
}

func main() {
	r := gin.Default()

	// 创建并配置/v1版本的API路由组。
	// 使用VersionMiddleware设置API版本为"1.0"。
	v1 := r.Group("/v1", VersionMiddleware("1.0"))
	{
		// 处理/v1版本的GET请求，返回用户列表信息和API版本。
		v1.GET("/users", func(c *gin.Context) {
			version := c.MustGet("api-version").(string)
			c.JSON(200, gin.H{"version": version, "message": "Users list"})
		})
	}

	// 创建并配置/v2版本的API路由组。
	// 使用VersionMiddleware设置API版本为"2.0"。
	v2 := r.Group("/v2", VersionMiddleware("2.0"))
	{
		// 处理/v2版本的GET请求，返回用户列表信息和API版本，可能包含新特性。
		v2.GET("/users", func(c *gin.Context) {
			version := c.MustGet("api-version").(string)
			c.JSON(200, gin.H{"version": version, "message": "Users list with new features"})
		})
	}

	// 启动HTTP服务器，监听端口8080。
	r.Run(":8080")
}
