package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// CustomRecovery 自定义异常恢复中间件
// 该中间件用于捕获并处理控制器中的 panic
// 返回: gin.HandlerFunc 类型的中间件函数
func CustomRecovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// 如果有 panic 发生，设置状态码为 500 并返回错误信息
				c.Error(err.(error)).SetMeta("recovered")
				c.AbortWithStatusJSON(http.StatusInternalServerError,
					gin.H{"error": "Internal Server Error"})
			}
		}()
		c.Next()
	}
}

func main() {
	r := gin.Default()

	// 添加全局异常恢复中间件
	r.Use(CustomRecovery())

	// 定义可能引发 panic 的路由
	r.GET("/panic", func(c *gin.Context) {
		panic("an error occurred!")
	})

	r.Run(":8080")
}
