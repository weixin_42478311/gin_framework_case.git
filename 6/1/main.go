package main

import (
	"github.com/gin-gonic/gin"
)

// main 是程序的入口点
func main() {
	// 创建一个默认的 Gin 路由器
	router := gin.Default()

	// 定义根路径的 GET 请求处理函数
	router.GET("/", func(c *gin.Context) {
		// 返回 HTTP 200 状态码和 "Hello World!" 字符串响应
		c.String(200, "Hello World!")
	})

	// 定义 /submit 路径的 POST 请求处理函数
	router.POST("/submit", func(c *gin.Context) {
		// 返回 HTTP 200 状态码和一个 JSON 响应，包含消息 "Form submitted"
		c.JSON(200, gin.H{
			"message": "Form submitted",
		})
	})

	// 监听并在 0.0.0.0:8080 上启动服务
	router.Run(":8080")
}
