package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func main() {
	// 创建默认的路由引擎
	router := gin.Default()

	// 创建一个名为 "api" 的分组路由，所有以 /api 开头的 URL 都会进入这个分组
	api := router.Group("/api")
	{
		// 为整个分组添加一个简单的日志中间件
		api.Use(func(c *gin.Context) {
			fmt.Println("API Middleware Called")
			c.Next()
		})

		// 定义一个 GET 路由，用于获取所有用户
		api.GET("/users", getUsers)

		// 定义一个带参数的 GET 路由，用于根据 ID 获取单个用户
		api.GET("/users/:id", getUserByID)
	}

	// 启动 HTTP 服务，默认监听端口是 8080
	router.Run(":8080")
}

// 模拟获取所有用户的处理器
func getUsers(c *gin.Context) {
	users := []string{"Alice", "Bob", "Charlie"}
	c.JSON(200, users)
}

// 模拟根据 ID 获取单个用户的处理器
func getUserByID(c *gin.Context) {
	id := c.Param("id")
	user := "User with ID " + id
	c.JSON(200, gin.H{"user": user})
}
