package main

import (
	"github.com/gin-gonic/gin"
)

// main 是程序的入口点
func main() {
	// 创建一个默认的 Gin 路由器
	router := gin.Default()

	// 路由处理函数，用于响应带有具体用户ID的GET请求
	router.GET("/user/:id", func(c *gin.Context) {
		// 提取URL参数中的id值
		id := c.Param("id")
		// 返回用户ID信息
		c.String(200, "User ID is %s", id)
	})

	// 使用通配符 :name 匹配任意字符直到斜杠或结尾
	router.GET("/file/*filepath", func(c *gin.Context) {
		// 提取URL参数中的filepath值
		filepath := c.Param("filepath")
		// 返回文件路径信息
		c.String(200, "File path is %s", filepath)
	})

	// 监听并在 0.0.0.0:8080 上启动服务
	router.Run(":8080")
}
