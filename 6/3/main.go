package main

import (
	"github.com/gin-gonic/gin"
)

// main 是程序的入口点
func main() {
	// 创建一个默认的 Gin 路由器
	router := gin.Default()

	// RouterGET为/search路径定义了一个处理函数，用于处理GET请求。
	// 该函数接收一个*gin.Context参数c，代表HTTP请求的上下文。
	// 参数c包含了请求、响应、HTTP头等信息，并且是处理请求和写入响应的核心对象。
	router.GET("/search", func(c *gin.Context) {
		// 使用c.Query方法从请求的查询字符串中获取名为'q'的值。
		// 如果'q'不存在，该方法返回一个空字符串。
		query := c.Query("q")
		// 使用c.String方法向HTTP响应中写入一个格式化后的字符串。
		// 该方法的参数包括HTTP状态码(此处为200，表示OK)和格式化字符串以及相应的参数。
		c.String(200, "Search query is %s", query)
	})

	// 监听并在 0.0.0.0:8080 上启动服务
	router.Run(":8080")
}
