package main

import (
	"github.com/gin-gonic/gin"
)

// main 是程序的入口点
func main() {
	// 创建一个默认的 Gin 路由器
	router := gin.Default()

	// 处理 POST 请求的路由
	router.POST("/form", func(c *gin.Context) {
		// 获取表单数据中的 name 字段
		name := c.PostForm("name")
		// 获取表单数据中的 age 字段
		age := c.PostForm("age")
		// 返回处理结果
		c.String(200, "Name: %s, Age: %s", name, age)
	})

	// 监听并在 0.0.0.0:8080 上启动服务
	router.Run(":8080")
}
