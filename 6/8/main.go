package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	// 创建默认的路由引擎
	router := gin.Default()

	// 定义一个 GET 路由，访问根路径时返回 "Hello World!"
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello World!")
	})

	// 定义一个可能引发错误的 GET 路由
	router.GET("/error", func(c *gin.Context) {
		// 模拟一个可能导致 panic 的操作
		panic("Something went wrong!")
	})

	// 自定义 404 页面
	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"code":    http.StatusNotFound,
			"message": "Sorry, the page you're looking for doesn't exist.",
		})
	})

	// 使用 recover 中间件来捕获任何导致 panic 的错误，并返回 500 状态码
	router.Use(func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// 如果发生 panic，记录错误信息并返回 500 状态码
				fmt.Printf("Panic occurred: %v\n", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"code":    http.StatusInternalServerError,
					"message": "Internal Server Error",
				})
			}
		}()
		c.Next()
	})

	// 启动 HTTP 服务，默认监听端口是 8080
	router.Run(":8080")
}
