package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	// 创建默认的路由引擎
	router := gin.Default()

	// 提供静态文件服务
	// 这里的第一个参数是 URL 路径前缀，第二个参数是本地文件系统中的目录路径。
	// 访问 http://localhost:8080/static/css/style.css 将会返回 ./static/css/style.css 文件
	router.Static("/static", "./static")

	// 定义一个简单的根路径处理器，用于展示如何链接到静态资源
	router.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.tmpl", nil)
	})

	// 加载 HTML 模板
	router.LoadHTMLGlob("templates/*")

	// 启动 HTTP 服务，默认监听端口是 8080
	router.Run(":8080")
}
