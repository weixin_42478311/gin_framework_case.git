package main

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	// 创建默认的路由引擎
	router := gin.Default()

	// 定义一个全局中间件，用于记录所有请求的时间戳和耗时
	router.Use(Logger())

	// 定义一个 GET 路由，访问根路径时返回 "Hello World!"
	router.GET("/", func(c *gin.Context) {
		c.String(200, "Hello World!")
	})

	// 定义另一个 GET 路由，模拟处理较慢的请求
	router.GET("/slow", func(c *gin.Context) {
		time.Sleep(2 * time.Second)
		c.String(200, "This was a slow request.")
	})

	// 启动 HTTP 服务，默认监听端口是 8080
	router.Run(":8080")
}

// Logger 是一个自定义的中间件函数
func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 记录开始时间
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		// 处理请求前的操作
		fmt.Printf("Started %s %s\n", c.Request.Method, path)

		// 将请求传递给下一个中间件或处理器
		c.Next()

		// 处理请求后的操作
		end := time.Since(start)
		fmt.Printf("Completed %s in %v\n", path, end)

		// 如果有查询参数，则打印出来
		if raw != "" {
			fmt.Printf("With query: %s\n", raw)
		}
	}
}
